EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "nes clone game pad"
Date "2020-01-28"
Rev "v1.0"
Comp "intotodesign"
Comment1 "designed by james weeks"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4650 3750 5200 3750
Text Label 5800 3750 0    50   ~ 0
GND
Wire Wire Line
	4450 1550 4200 1550
Wire Wire Line
	1550 5800 1550 6100
Wire Wire Line
	4650 6100 1550 6100
Wire Wire Line
	4650 3750 4650 6100
Text Label 1550 5950 0    50   ~ 0
GND
Connection ~ 1550 5800
Wire Wire Line
	2100 5800 4450 5800
Connection ~ 1550 5350
Wire Wire Line
	1550 5800 1700 5800
Wire Wire Line
	1550 5350 1550 5800
Connection ~ 1550 4900
Wire Wire Line
	1550 5350 1700 5350
Wire Wire Line
	1550 4900 1550 5350
Connection ~ 1550 4500
Wire Wire Line
	1550 4900 1700 4900
Wire Wire Line
	1550 4500 1550 4900
Connection ~ 1550 4100
Wire Wire Line
	1550 4500 1700 4500
Wire Wire Line
	1550 4100 1550 4500
Connection ~ 1550 3700
Wire Wire Line
	1550 4100 1700 4100
Wire Wire Line
	1550 3700 1550 4100
Connection ~ 1550 3250
Wire Wire Line
	1550 3700 1700 3700
Wire Wire Line
	1550 3250 1550 3700
Wire Wire Line
	1550 3250 1700 3250
Wire Wire Line
	1550 2800 1550 3250
Wire Wire Line
	1700 2800 1550 2800
Wire Wire Line
	2100 2800 2100 2850
Connection ~ 4950 1450
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5E371819
P 4950 1450
F 0 "#FLG0101" H 4950 1525 50  0001 C CNN
F 1 "PWR_FLAG" H 4950 1623 50  0000 C CNN
F 2 "" H 4950 1450 50  0001 C CNN
F 3 "~" H 4950 1450 50  0001 C CNN
	1    4950 1450
	1    0    0    -1  
$EndComp
Text Label 5950 4900 0    50   ~ 0
VDD
NoConn ~ 5850 4900
NoConn ~ 5750 4900
Text Label 5650 4900 3    50   ~ 0
DATA
Text Label 5550 4900 3    50   ~ 0
LATCH
Text Label 5450 4900 3    50   ~ 0
CLOCK
Text Label 5350 4900 3    50   ~ 0
GND
NoConn ~ 6200 2450
NoConn ~ 6200 2350
Text Label 6200 2550 0    50   ~ 0
DATA
Text Label 2600 1400 0    50   ~ 0
VDD
Connection ~ 2600 1550
Wire Wire Line
	2600 1550 2600 1400
Text Label 4750 1800 1    50   ~ 0
CLOCK
Text Label 5450 1800 0    50   ~ 0
LATCH
Wire Wire Line
	4950 1950 4950 1900
Connection ~ 4950 1950
Wire Wire Line
	4750 1950 4750 1800
Wire Wire Line
	4950 1950 4750 1950
Wire Wire Line
	5200 1950 5200 1900
Connection ~ 5200 1950
Wire Wire Line
	5450 1950 5450 1800
Wire Wire Line
	5200 1950 5450 1950
Wire Wire Line
	5150 1450 4950 1450
Connection ~ 5150 1450
Wire Wire Line
	5150 1250 5150 1450
Wire Wire Line
	4950 1450 4950 1600
Wire Wire Line
	5200 1450 5150 1450
Wire Wire Line
	5200 1600 5200 1450
Wire Wire Line
	5200 2350 5400 2350
Wire Wire Line
	5200 2350 5200 1950
Wire Wire Line
	4950 2550 5400 2550
Wire Wire Line
	4950 2550 4950 1950
Text Label 5150 1250 2    50   ~ 0
VDD
Text Label 5400 2650 2    50   ~ 0
GND
Wire Wire Line
	4450 3550 4450 2050
Connection ~ 4450 3550
Wire Wire Line
	5400 3550 4450 3550
Wire Wire Line
	4200 3450 4200 5350
Connection ~ 4200 3450
Wire Wire Line
	4200 2050 4200 3450
Wire Wire Line
	5400 3450 4200 3450
Wire Wire Line
	3950 3350 3950 4900
Connection ~ 3950 3350
Wire Wire Line
	3950 2050 3950 3350
Wire Wire Line
	5400 3350 3950 3350
Wire Wire Line
	3700 3250 3700 4500
Connection ~ 3700 3250
Wire Wire Line
	5400 3250 3700 3250
Wire Wire Line
	3400 3150 3400 2050
Connection ~ 3400 3150
Wire Wire Line
	5400 3150 3400 3150
Wire Wire Line
	3150 3050 3150 3700
Connection ~ 3150 3050
Wire Wire Line
	5400 3050 3150 3050
Wire Wire Line
	3400 4100 3400 3150
Wire Wire Line
	3700 2050 3700 3250
Wire Wire Line
	2900 2950 2900 2050
Connection ~ 2900 2950
Wire Wire Line
	2600 2050 2600 2850
Wire Wire Line
	5400 2950 2900 2950
Connection ~ 2600 2850
Wire Wire Line
	5400 2850 2600 2850
Wire Wire Line
	2900 3250 2900 2950
Wire Wire Line
	2100 3250 2900 3250
Wire Wire Line
	3150 3700 2100 3700
Wire Wire Line
	3150 2050 3150 3050
Wire Wire Line
	2100 4100 3400 4100
Wire Wire Line
	3700 4500 2100 4500
Wire Wire Line
	2100 4900 3950 4900
Wire Wire Line
	4200 5350 2100 5350
Wire Wire Line
	4450 5800 4450 3550
Wire Wire Line
	2100 2850 2600 2850
Wire Wire Line
	4200 1550 3950 1550
Connection ~ 4200 1550
Wire Wire Line
	4200 1750 4200 1550
Wire Wire Line
	3950 1550 3700 1550
Connection ~ 3950 1550
Wire Wire Line
	3950 1750 3950 1550
Wire Wire Line
	3700 1550 3400 1550
Connection ~ 3700 1550
Wire Wire Line
	3700 1750 3700 1550
Wire Wire Line
	3400 1550 3150 1550
Connection ~ 3400 1550
Wire Wire Line
	3400 1750 3400 1550
Wire Wire Line
	3150 1550 2900 1550
Connection ~ 3150 1550
Wire Wire Line
	3150 1750 3150 1550
Wire Wire Line
	2900 1550 2600 1550
Connection ~ 2900 1550
Wire Wire Line
	2900 1750 2900 1550
Wire Wire Line
	2600 1550 2600 1750
Wire Wire Line
	4450 1750 4450 1550
$Comp
L Switch:SW_Push SW6
U 1 1 5E314463
P 1900 5350
F 0 "SW6" H 1900 5635 50  0000 C CNN
F 1 "B" H 1900 5544 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_SPST_NO_Alps_SKRK" H 1900 5550 50  0001 C CNN
F 3 "~" H 1900 5550 50  0001 C CNN
	1    1900 5350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x07_Female J1
U 1 1 5E318A30
P 5650 4700
F 0 "J1" V 5815 4680 50  0000 C CNN
F 1 "Conn_01x05_Female" V 5724 4680 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x07_P2.54mm_Vertical" H 5650 4700 50  0001 C CNN
F 3 "~" H 5650 4700 50  0001 C CNN
	1    5650 4700
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R8
U 1 1 5E318734
P 4450 1900
F 0 "R8" H 4520 1946 50  0000 L CNN
F 1 "R" H 4520 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4380 1900 50  0001 C CNN
F 3 "~" H 4450 1900 50  0001 C CNN
	1    4450 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5E3184C6
P 4200 1900
F 0 "R7" H 4270 1946 50  0000 L CNN
F 1 "R" H 4270 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4130 1900 50  0001 C CNN
F 3 "~" H 4200 1900 50  0001 C CNN
	1    4200 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5E31836D
P 3950 1900
F 0 "R6" H 4020 1946 50  0000 L CNN
F 1 "R" H 4020 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3880 1900 50  0001 C CNN
F 3 "~" H 3950 1900 50  0001 C CNN
	1    3950 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5E3180A0
P 3700 1900
F 0 "R5" H 3770 1946 50  0000 L CNN
F 1 "R" H 3770 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3630 1900 50  0001 C CNN
F 3 "~" H 3700 1900 50  0001 C CNN
	1    3700 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5E317E46
P 3400 1900
F 0 "R4" H 3470 1946 50  0000 L CNN
F 1 "R" H 3470 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3330 1900 50  0001 C CNN
F 3 "~" H 3400 1900 50  0001 C CNN
	1    3400 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5E317C55
P 3150 1900
F 0 "R3" H 3220 1946 50  0000 L CNN
F 1 "R" H 3220 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3080 1900 50  0001 C CNN
F 3 "~" H 3150 1900 50  0001 C CNN
	1    3150 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5E3179D7
P 2900 1900
F 0 "R2" H 2970 1946 50  0000 L CNN
F 1 "R" H 2970 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 2830 1900 50  0001 C CNN
F 3 "~" H 2900 1900 50  0001 C CNN
	1    2900 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5E31754F
P 2600 1900
F 0 "R1" H 2670 1946 50  0000 L CNN
F 1 "R" H 2670 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 2530 1900 50  0001 C CNN
F 3 "~" H 2600 1900 50  0001 C CNN
	1    2600 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 5E316596
P 4950 1750
F 0 "R9" H 4880 1704 50  0000 R CNN
F 1 "R" H 4880 1795 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4880 1750 50  0001 C CNN
F 3 "~" H 4950 1750 50  0001 C CNN
	1    4950 1750
	-1   0    0    1   
$EndComp
$Comp
L Device:R R10
U 1 1 5E31626A
P 5200 1750
F 0 "R10" H 5130 1704 50  0000 R CNN
F 1 "R" H 5130 1795 50  0000 R CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 5130 1750 50  0001 C CNN
F 3 "~" H 5200 1750 50  0001 C CNN
	1    5200 1750
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_Push SW7
U 1 1 5E31406A
P 1900 5800
F 0 "SW7" H 1900 6085 50  0000 C CNN
F 1 "A" H 1900 5994 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_SPST_NO_Alps_SKRK" H 1900 6000 50  0001 C CNN
F 3 "~" H 1900 6000 50  0001 C CNN
	1    1900 5800
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SELECT1
U 1 1 5E313D2E
P 1900 4900
F 0 "SELECT1" H 1900 5185 50  0000 C CNN
F 1 "SW_Push" H 1900 5094 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_SPST_NO_Alps_SKRK" H 1900 5100 50  0001 C CNN
F 3 "~" H 1900 5100 50  0001 C CNN
	1    1900 4900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 5E313A8A
P 1900 4500
F 0 "SW5" H 1900 4785 50  0000 C CNN
F 1 "START" H 1900 4694 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_SPST_NO_Alps_SKRK" H 1900 4700 50  0001 C CNN
F 3 "~" H 1900 4700 50  0001 C CNN
	1    1900 4500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5E3136D3
P 1900 3700
F 0 "SW3" H 1900 3985 50  0000 C CNN
F 1 "DOWN" H 1900 3894 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_SPST_NO_Alps_SKRK" H 1900 3900 50  0001 C CNN
F 3 "~" H 1900 3900 50  0001 C CNN
	1    1900 3700
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 5E313329
P 1900 4100
F 0 "SW4" H 1900 4385 50  0000 C CNN
F 1 "UP" H 1900 4294 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_SPST_NO_Alps_SKRK" H 1900 4300 50  0001 C CNN
F 3 "~" H 1900 4300 50  0001 C CNN
	1    1900 4100
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5E313089
P 1900 3250
F 0 "SW2" H 1900 3535 50  0000 C CNN
F 1 "LEFT" H 1900 3444 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_SPST_NO_Alps_SKRK" H 1900 3450 50  0001 C CNN
F 3 "~" H 1900 3450 50  0001 C CNN
	1    1900 3250
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5E313006
P 1900 2800
F 0 "SW1" H 1900 3085 50  0000 C CNN
F 1 "RIGHT" H 1900 2994 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_SPST_NO_Alps_SKRK" H 1900 3000 50  0000 C CNN
F 3 "~" H 1900 3000 50  0001 C CNN
	1    1900 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5E35DA80
P 5200 3750
F 0 "#PWR0101" H 5200 3500 50  0001 C CNN
F 1 "GND" H 5205 3577 50  0000 C CNN
F 2 "" H 5200 3750 50  0001 C CNN
F 3 "" H 5200 3750 50  0001 C CNN
	1    5200 3750
	1    0    0    -1  
$EndComp
Connection ~ 5200 3750
Wire Wire Line
	5200 3750 5800 3750
$Comp
L Device:LED D1
U 1 1 5E366919
P 6700 1400
F 0 "D1" H 6693 1145 50  0000 C CNN
F 1 "LED" H 6693 1236 50  0000 C CNN
F 2 "LED_SMD:LED_2816_7142Metric_Pad3.20x4.45mm_HandSolder" H 6700 1400 50  0001 C CNN
F 3 "~" H 6700 1400 50  0001 C CNN
	1    6700 1400
	-1   0    0    1   
$EndComp
$Comp
L Device:R R11
U 1 1 5E367319
P 7050 1400
F 0 "R11" V 6843 1400 50  0000 C CNN
F 1 "R" V 6934 1400 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6980 1400 50  0001 C CNN
F 3 "~" H 7050 1400 50  0001 C CNN
	1    7050 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	6850 1400 6900 1400
Wire Wire Line
	6550 1400 6550 1700
Wire Wire Line
	7200 1400 7200 1700
Text Label 7200 1700 0    50   ~ 0
VDD
Text Label 6550 1700 0    50   ~ 0
GND
Text Label 5800 2150 0    50   ~ 0
VDD
$Comp
L 4xxx:4021 U1
U 1 1 5E3156FC
P 5800 2950
F 0 "U1" H 5800 3931 50  0000 C CNN
F 1 "4021" H 5800 3840 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 5800 3100 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/HEF4021B.pdf" H 5800 3100 50  0001 C CNN
	1    5800 2950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
