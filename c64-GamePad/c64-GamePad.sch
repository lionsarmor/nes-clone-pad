EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "c64-gamepad"
Date "2020-01-28"
Rev "v1.0"
Comp "intotodesign"
Comment1 "compatible gamepad, c64/7800/2600"
Comment2 "made by James weeks"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_Push SW1
U 1 1 5E30EC0F
P 1150 1850
F 0 "SW1" H 1150 2135 50  0000 C CNN
F 1 "SW_Push UP" H 1150 2044 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 1150 2050 50  0001 C CNN
F 3 "~" H 1150 2050 50  0001 C CNN
	1    1150 1850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 5E30F839
P 1950 1850
F 0 "SW2" H 1950 2135 50  0000 C CNN
F 1 "SW_Push DOWN" H 1950 2044 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 1950 2050 50  0001 C CNN
F 3 "~" H 1950 2050 50  0001 C CNN
	1    1950 1850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 5E30FDC4
P 3450 1850
F 0 "SW4" H 3450 2135 50  0000 C CNN
F 1 "SW_Push Right" H 3450 2044 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 3450 2050 50  0001 C CNN
F 3 "~" H 3450 2050 50  0001 C CNN
	1    3450 1850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW6
U 1 1 5E3101E3
P 5200 2400
F 0 "SW6" H 5200 2685 50  0000 C CNN
F 1 "SW_Push Right Value" H 5200 2594 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 5200 2600 50  0001 C CNN
F 3 "~" H 5200 2600 50  0001 C CNN
	1    5200 2400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 5E31052A
P 4500 2400
F 0 "SW5" H 4500 2685 50  0000 C CNN
F 1 "SW_Push Left Fire" H 4500 2594 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 4500 2600 50  0001 C CNN
F 3 "~" H 4500 2600 50  0001 C CNN
	1    4500 2400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 5E31FDE6
P 2750 1850
F 0 "SW3" H 2750 2135 50  0000 C CNN
F 1 "SW_Push Left" H 2750 2044 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 2750 2050 50  0001 C CNN
F 3 "~" H 2750 2050 50  0001 C CNN
	1    2750 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  1850 950  1350
Wire Wire Line
	950  1350 1750 1350
Wire Wire Line
	1750 1350 1750 1850
Wire Wire Line
	1750 1350 2550 1350
Wire Wire Line
	2550 1350 2550 1850
Connection ~ 1750 1350
Wire Wire Line
	3250 1850 3250 1350
Wire Wire Line
	3250 1350 2550 1350
Connection ~ 2550 1350
$Comp
L Device:R R1
U 1 1 5E319A23
P 2650 3000
F 0 "R1" H 2720 3046 50  0000 L CNN
F 1 "620ohm/520ohm" H 2720 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 2580 3000 50  0001 C CNN
F 3 "~" H 2650 3000 50  0001 C CNN
	1    2650 3000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x10_Female J1
U 1 1 5E30FFF8
P 2500 3700
F 0 "J1" V 2665 3630 50  0000 C CNN
F 1 "Conn_01x10_Female" V 2574 3630 50  0000 C CNN
F 2 "Connector_PinSocket_1.00mm:PinSocket_1x10_P1.00mm_Vertical" H 2500 3700 50  0001 C CNN
F 3 "~" H 2500 3700 50  0001 C CNN
	1    2500 3700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2100 3900 2100 2150
Wire Wire Line
	2100 2150 1350 2150
Wire Wire Line
	1350 2150 1350 1850
Wire Wire Line
	2150 1850 2150 3900
Wire Wire Line
	2150 3900 2200 3900
Wire Wire Line
	2300 2050 2950 2050
Wire Wire Line
	2950 2050 2950 1850
Wire Wire Line
	2400 3900 2400 2200
Wire Wire Line
	2400 2200 3650 2200
Wire Wire Line
	3650 2200 3650 1850
Wire Wire Line
	2800 3900 2800 4050
Wire Wire Line
	2800 4050 950  4050
Wire Wire Line
	950  4050 950  1850
Connection ~ 950  1850
Wire Wire Line
	2500 3900 2500 2250
Wire Wire Line
	3800 2250 3800 1950
Wire Wire Line
	3800 1950 5400 1950
Wire Wire Line
	5400 1950 5400 2400
Wire Wire Line
	2600 2400 2600 3900
Wire Wire Line
	4300 2400 2600 2400
Wire Wire Line
	5000 2400 5000 2050
Wire Wire Line
	5000 2050 3900 2050
Wire Wire Line
	3900 2050 3900 2350
Wire Wire Line
	3900 2350 2600 2350
Wire Wire Line
	2600 2350 2600 2400
Connection ~ 2600 2400
Wire Wire Line
	4700 2400 4700 2550
Wire Wire Line
	2650 2550 2650 2850
Connection ~ 2900 2550
Wire Wire Line
	2900 2550 2650 2550
Wire Wire Line
	4700 2550 2900 2550
Wire Wire Line
	2500 2250 3800 2250
Wire Wire Line
	2300 3900 2300 2050
Wire Wire Line
	2900 2550 2900 3900
$Comp
L Device:R R2
U 1 1 5E31A583
P 3300 3000
F 0 "R2" H 3370 3046 50  0000 L CNN
F 1 "620ohm/520ohm" H 3370 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 3230 3000 50  0001 C CNN
F 3 "~" H 3300 3000 50  0001 C CNN
	1    3300 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 2400 5400 2850
Wire Wire Line
	5400 2850 3300 2850
Connection ~ 5400 2400
Wire Wire Line
	3300 3150 3300 4050
Wire Wire Line
	3300 4050 2800 4050
Connection ~ 2800 4050
NoConn ~ 2700 3900
NoConn ~ 3000 3900
Wire Wire Line
	2650 3150 2800 3150
Wire Wire Line
	2800 3150 2800 3900
Connection ~ 2800 3900
Wire Wire Line
	2800 3900 2800 4050
$EndSCHEMATC
